#include "SharedMemory.h"
#include <sys/stat.h>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <errno.h>

#include <cstdio>

soi::SharedMemory::SharedMemory(size_t size)
    : segmentId(shmget(IPC_PRIVATE, size, IPC_CREAT | IPC_EXCL | 0666)), // 0666 - read, write for all
      creatorPid(getpid()),
      lastUserPid(-1)
{
    if(segmentId == -1)
    {
        perror("Failed to allocate shared memory segment.");
        std::exit(-1);
    }
}

soi::SharedMemory::~SharedMemory()
{
    pid_t pid = getpid();
    if(pid == lastUserPid)
    {
        shmdt(data);
        //printf("shared memory detached in process %i\n", pid);
    }
    if(pid == creatorPid)
    {
        shmctl(segmentId, IPC_RMID, 0);
        //printf("shared memory deleted in process %i\n", pid);
    }
}

void* soi::SharedMemory::getData()
{
    pid_t pid = getpid();
    if(pid == lastUserPid)
        return data;
    lastUserPid = pid;
    void *tmp = shmat(segmentId, 0, 0);
    if((long int)tmp == -1)
    {
        perror("Failed to attach shared memory segment.");
        std::exit(-1);
    }
    //printf("shared memory attached in process %i\n", pid);
    data = tmp;
    return tmp;
}
