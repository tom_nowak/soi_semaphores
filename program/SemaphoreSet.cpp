#include "SemaphoreSet.h"
#include <errno.h>
#include <cstdio>
#include <cstdlib>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>

namespace
{
    union semun
    {
        int val;
        struct semid_ds *buf;
        unsigned short int *array;
        struct seminfo *__buf;
    };
}

soi::SemaphoreSet::SemaphoreSet(std::initializer_list<unsigned short> values)
    : semId(semget(IPC_PRIVATE, values.size(), IPC_CREAT | IPC_EXCL | 0666)), // 0666 - read, write for all
      creatorPid(getpid())
{
    if(semId == -1)
    {
        perror("Failed to create semaphore set");
        std::exit(-1);
    }
    semun argument; // required by semctl
    std::vector<unsigned short> val(std::move(values));
    argument.array = val.data();
    if(semctl(semId, 0, SETALL, argument) == -1) // set all semaphore values according to argument.array
    {
        perror("Failed to initialize semaphore set values");
        std::exit(-1);
    }
    // printf only for testing, should not be used in general
    fprintf(stderr, "Created semaphore set in process %i. Values:\n"
           "ITERATIONS_COUNTER: %i\n"
           "FREE_SERVICE_STATIONS: %i\n"
           "CRITICAL_SECTION: %i\n"
           "ENTRANCE: %i\n"
           "EXIT: %i\n\n",
           creatorPid, getValue(0), getValue(1), getValue(2), getValue(3), getValue(4));
}

soi::SemaphoreSet::~SemaphoreSet()
{
    union semun ignoredArgument;
    if(getpid() == creatorPid)
    {
        // printf only for testing, should not be used in general
        fprintf(stderr, "Deleting semaphore set in process %i. Values:\n"
               "ITERATIONS_COUNTER: %i\n"
               "FREE_SERVICE_STATIONS: %i\n"
               "CRITICAL_SECTION: %i\n"
               "ENTRANCE: %i\n"
               "EXIT: %i\n\n",
               creatorPid, getValue(0), getValue(1), getValue(2), getValue(3), getValue(4));
        semctl(semId, 1, IPC_RMID, ignoredArgument);
    }
}

int soi::SemaphoreSet::getValue(unsigned short semaphoreNumber)
{
    return semctl(semId, semaphoreNumber, GETVAL);
}

void soi::SemaphoreSet::acquire(unsigned short semaphoreNumber)
{
    sembuf operations[1];
    operations[0].sem_num = semaphoreNumber;
    operations[0].sem_op = -1; // decrement by 1
    operations[0].sem_flg = 0; // no SEM_UNDO, nor IPC_NOWAIT
    //fprintf(stderr, "acquire semaphore %hu\n", semaphoreNumber); // debug printf for deadlocks
    if(semop(semId, operations, 1) == -1)
    {
        perror("Failed to acquire semaphore");
        std::exit(-1);
    }
}

void soi::SemaphoreSet::release(unsigned short semaphoreNumber)
{
    sembuf operations[1];
    operations[0].sem_num = semaphoreNumber;
    operations[0].sem_op = 1; // increment by 1
    operations[0].sem_flg = 0; // no SEM_UNDO, nor IPC_NOWAIT
    if(semop(semId, operations, 1) == -1)
    {
        perror("Failed to release semaphore");
        std::exit(-1);
    }
}
