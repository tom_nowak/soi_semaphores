#pragma once

namespace soi
{
    // Each of bolid activities (driving, being on runway, being in service station)
    // will take some time: sleep(rand() % (max-min)) + min
    // TimeParams stores these times. In settings they are saved in milliseconds,
    // but values are stored in microseconds, because there is function usleep(microseconds)
    struct TimeParams
    {
        unsigned roadwayMin;
        unsigned roadwayMax;
        unsigned runwayMin;
        unsigned runwayMax;
        unsigned serviceMin;
        unsigned serviceMax;

        bool isCorrect();
    };

    class Settings
    {
    public:
        Settings(const char *filename);
        inline unsigned short getNumberOfBolids() const { return numberOfBolids; }
        inline unsigned short getNumberOfServiceStations() const { return numberOfServiceStations; }
        inline unsigned short getPriorityThreshold() const { return priorityThreshold; }
        inline unsigned short getNumberOfIterationsToEnd() const { return numberOfIterationsToEnd; }
        inline const TimeParams& getTimes() const { return timeParams; }

    private:
        // type is unsigned short, because system V semaphores have unsigned short values
        unsigned short numberOfBolids;
        unsigned short numberOfServiceStations;
        unsigned short priorityThreshold;
        unsigned short numberOfIterationsToEnd;
        TimeParams timeParams;
    };
}
