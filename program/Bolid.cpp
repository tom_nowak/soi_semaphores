#include "Bolid.h"
#include "SemaphoreSet.h"
#include "SharedData.h"
#include <cstdio>
#include <unistd.h>
#include <cstdlib>
#include <ctime>

soi::Bolid::Bolid(SharedData &d, SemaphoreSet &semset, const Settings &s)
    : shd(d), sem(semset), settings(s), lastServiceStationUsed(-1),
      numberOfIterations(0), timeRoadway(0), timeRunway(0), timeService(0),
      tRoadwayDiff(s.getTimes().roadwayMax - s.getTimes().roadwayMin),
      tRunwayDiff(s.getTimes().runwayMax - s.getTimes().runwayMin),
      tServiceDiff(s.getTimes().serviceMax - s.getTimes().serviceMin)
{
    std::srand(std::time(NULL) ^ (getpid()<<8)); // to make srand different among processes
    gettimeofday(&startTime, NULL);
}

soi::Bolid::~Bolid()
{
    timeval tv;
    gettimeofday(&tv, NULL);
    float t =  (float)(1000000*(tv.tv_sec - startTime.tv_sec) + tv.tv_usec - startTime.tv_usec );
    t *= 0.001f/(float)numberOfIterations; // compute average time and change to milliseconds
    float td = 0.001f*(float)timeRoadway/(float)numberOfIterations; // 0.001f - change to milliseconds
    float tr = 0.001f*(float)timeRunway/(2*(float)numberOfIterations); // 2x driveOnRunway() for each iteration
    float ts = 0.001f*(float)timeService/(float)numberOfIterations;
    // note that t has nothing to do with td+tr+ts, because t is not only sleeping, but also waiting on semaphores etc.
    sem.acquire(semaphore_index::CRITICAL_SECTION); // printing reports should be atomic, as there are other bolids running
    fprintf(stderr, "Bolid %i finished running. Report (times in milliseconds):\n"
           "numberOfIterations:              %u\n"
           "average time on roadway:         %.2f\n"
           "average time on runway:          %.2f\n"
           "average time at service station: %.2f\n"
           "average iteration time:          %.2f\n"
           "service stations vector:         ",
           getpid(), numberOfIterations, td, tr, ts, t);
    for(int i=0; i<settings.getNumberOfServiceStations(); ++i)
        fprintf(stderr, "%i", shd.getServiceStation(i));
    fprintf(stderr, "\n\n");
    sem.release(semaphore_index::CRITICAL_SECTION);
}

void soi::Bolid::run()
{
    do
    {
        driveOnRoad();
        goFromRoadToRunway();
        driveOnRunway();
        goFromRunwayToServiceStation();
        stayAtServiceStation();
        goFromServiceStationToRunway();
        driveOnRunway();
        goFromRunwayToRoad();
    } while(!shouldFinish());
}

void soi::Bolid::driveOnRoad()
{
    unsigned n = std::rand() % tRoadwayDiff + settings.getTimes().roadwayMin;
    timeRoadway += n;
    usleep(n);
}

void soi::Bolid::goFromRoadToRunway()
{
    sem.acquire(semaphore_index::FREE_SERVICE_STATIONS);
    sem.acquire(semaphore_index::CRITICAL_SECTION);
    while(!shd.isRunwayFree)
    {
        ++shd.numberOfBolidsWaitingForEntrance;
        sem.release(semaphore_index::CRITICAL_SECTION);
        sem.acquire(semaphore_index::ENTRANCE);
        if(sem.getValue(semaphore_index::CRITICAL_SECTION) > 0) // debug
            fprintf(stderr, "error, critical section free after waking up goFromRoadToRunway\n");
        --shd.numberOfBolidsWaitingForEntrance;
        if(!shd.isRunwayFree) // just for test/research
            fprintf(stderr, "spurious wakeup in goFromRoadToRunway!\n");
    }
    shd.isRunwayFree = 0;
    sem.release(semaphore_index::CRITICAL_SECTION);
}

void soi::Bolid::driveOnRunway()
{
    unsigned n = std::rand() % tRunwayDiff + settings.getTimes().runwayMin;
    timeRunway += n;
    usleep(n);
}

void soi::Bolid::goFromRunwayToServiceStation()
{
    sem.acquire(semaphore_index::CRITICAL_SECTION);
    shd.isRunwayFree = 1;
    if(shd.numberOfOccupiedServiceStations >= settings.getPriorityThreshold() &&
       shd.numberOfBolidsWaitingForExit > 0)
    {
        sem.release(semaphore_index::EXIT);
        sem.acquire(semaphore_index::CRITICAL_SECTION);
    }
    else if(shd.numberOfBolidsWaitingForEntrance > 0)
    {
        sem.release(semaphore_index::ENTRANCE);
        sem.acquire(semaphore_index::CRITICAL_SECTION);
    }
    else if(shd.numberOfBolidsWaitingForExit > 0)
    {
        sem.release(semaphore_index::EXIT);
        sem.acquire(semaphore_index::CRITICAL_SECTION);
    }
    for(unsigned i=0;; ++i) // look for free service station
    {
        char &c = shd.getServiceStation(i);
        if(c == 0) // service station is free
        {
            c = 1; // mark service station - occupied
            lastServiceStationUsed = i; // save - to know which service station to release
            break;
        }
        if(i == settings.getNumberOfServiceStations())
        {
            // this should not happen (there is semaphore FREE_SERVICE_STATIONS)
            fprintf(stderr, "error, no service station available!\n");
            break;
        }
    }
    ++shd.numberOfOccupiedServiceStations;
    sem.release(semaphore_index::CRITICAL_SECTION);
}

void soi::Bolid::stayAtServiceStation()
{
    unsigned n = std::rand() % tServiceDiff + settings.getTimes().serviceMin;
    timeService += n;
    usleep(n);
}

void soi::Bolid::goFromServiceStationToRunway()
{
    sem.acquire(semaphore_index::CRITICAL_SECTION);
    while(!shd.isRunwayFree)
    {
        ++shd.numberOfBolidsWaitingForExit;
        sem.release(semaphore_index::CRITICAL_SECTION);
        sem.acquire(semaphore_index::EXIT);
        if(sem.getValue(semaphore_index::CRITICAL_SECTION) > 0) // debug
            fprintf(stderr, "error, critical section free after waking up goFromServiceStationToRunway\n");
        --shd.numberOfBolidsWaitingForExit;
        if(!shd.isRunwayFree) // just for test/research
            fprintf(stderr, "spurious wakeup in goFromServiceStationToRunway!\n");
    }
    shd.isRunwayFree = 0;
    shd.getServiceStation(lastServiceStationUsed) = 0; // release service station
    --shd.numberOfOccupiedServiceStations;
    sem.release(semaphore_index::CRITICAL_SECTION);
}

void soi::Bolid::goFromRunwayToRoad()
{
    sem.acquire(semaphore_index::CRITICAL_SECTION);
    shd.isRunwayFree = 1;
    // if-else sequence similiar to goFromRunwayToServiceStation(),
    // but critical section is not acquired again (it is not needed)
    if(shd.numberOfOccupiedServiceStations >= settings.getPriorityThreshold() &&
       shd.numberOfBolidsWaitingForExit > 0)
        sem.release(semaphore_index::EXIT);
    else if(shd.numberOfBolidsWaitingForEntrance > 0)
        sem.release(semaphore_index::ENTRANCE);
    else if(shd.numberOfBolidsWaitingForExit > 0)
        sem.release(semaphore_index::EXIT);
    else
        sem.release(semaphore_index::CRITICAL_SECTION);
    sem.release(semaphore_index::FREE_SERVICE_STATIONS);
}

bool soi::Bolid::shouldFinish()
{
    ++numberOfIterations;
    sem.release(semaphore_index::ITERATIONS_COUNTER);
    int i = sem.getValue(semaphore_index::ITERATIONS_COUNTER);
    return i >= settings.getNumberOfIterationsToEnd();
}
