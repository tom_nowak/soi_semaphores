#include "Bolid.h"
#include "Settings.h"
#include "SharedMemory.h"
#include "SharedData.h"
#include "SemaphoreSet.h"
#include <unistd.h> // fork
#include <sys/wait.h> // wait
#include <cstdio>

using namespace soi;

int main(int argc, char **argv)
{
    if(argc != 2)
    {
        printf("Usage: %s <path to file with settings>\n", argv[0]);
        return 1;
    }
    Settings settings(argv[1]);
    // Allocate shared memory, i.e. SharedData object and array of flags (char) for each service station
    SharedMemory shm(sizeof(SharedData) + settings.getNumberOfServiceStations()*sizeof(char));
    ((SharedData*)shm.getData())->initialize(settings);
    // Create SemaphoreSet with starting values for each semaphore (see semaphore_index in SharedData.h)
    SemaphoreSet sset = { 0, settings.getNumberOfServiceStations(), 1, 0, 0 };
    for(unsigned i = 1; i < settings.getNumberOfBolids(); ++i)
    {
        if(fork() == 0) // child
        {
            Bolid(*(SharedData*)shm.getData(), sset, settings).run();
            return 0; // each child process only runs one bolid
        }
    }
    Bolid(*(SharedData*)shm.getData(), sset, settings).run(); // parent process will also have a bolid
    while(wait((int*)0) != -1);
    return 0;
}
