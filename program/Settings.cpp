#include "Settings.h"
#include <cstdio>
#include <cstdlib>

bool soi::TimeParams::isCorrect()
{
    return (roadwayMin <= roadwayMax) &&
           (runwayMin <= runwayMax) &&
           (serviceMin <= serviceMax);
}

soi::Settings::Settings(const char *filename)
{
#define READ(x, format) \
    if(fscanf(file, format, &x) != 1) \
    { \
        fprintf(stderr, "incorrect contents of settings file (unable to read data)\n"); \
        exit(-1); \
    } \
    if(x == 0) \
    { \
        fprintf(stderr, "incorrect contents of settings file (data cannot be 0)\n"); \
        exit(-1); \
    } \
    for(char c = fgetc(file); c!='\n' && c!=EOF; c = fgetc(file));

    FILE *file = fopen(filename, "r");
    if(!file)
        perror("could not open settings file");
    READ(numberOfBolids, "%hu")
    READ(numberOfServiceStations, "%hu")
    READ(priorityThreshold, "%hu")
    READ(numberOfIterationsToEnd, "%hu")
    READ(timeParams.roadwayMin, "%u")
    READ(timeParams.roadwayMax, "%u")
    READ(timeParams.runwayMin, "%u")
    READ(timeParams.runwayMax, "%u")
    READ(timeParams.serviceMin, "%u")
    READ(timeParams.serviceMax, "%u")
    fclose(file);
    if(!timeParams.isCorrect())
    {
        fprintf(stderr, "incorrect contents of settings file (time max is less than time min)\n");
        exit(-1);
    }
    // convert times from milliseconds to microseconds:
    timeParams.roadwayMin *= 1000;
    timeParams.roadwayMax *= 1000;
    timeParams.runwayMin *= 1000;
    timeParams.runwayMax *= 1000;
    timeParams.serviceMin *= 1000;
    timeParams.serviceMax *= 1000;
#undef READ
}
