#pragma once
#include <sys/shm.h>

namespace soi
{
    // SharedMemory object should be constructed once in one process, before using fork().
    // After each fork() call, new process should attach SharedMemory - in each of running processes
    // this class will be different, because data might be attached at different addresses.
    class SharedMemory
    {
    public:
        SharedMemory(size_t size); // should be used before forking processes
        ~SharedMemory(); // detaches memory in case of normal users, removes it in case of creator
        void* getData(); // returns data, at first call attaches memory before returning data

    private:
        const int segmentId; // uniqe ID of allocated memory
        const pid_t creatorPid; // pid of process in which shared memory has been constructed
        pid_t lastUserPid; // pid of last process which called getData()
        void *data; // pointer to last value returned by getData()
    };
}
