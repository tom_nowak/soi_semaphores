#pragma once
#include "Settings.h"

namespace soi
{
    struct SharedData
    {
        unsigned numberOfBolidsWaitingForEntrance;
        unsigned numberOfBolidsWaitingForExit;
        unsigned numberOfOccupiedServiceStations;
        bool isRunwayFree;
        //char serviceStationsArray[numberOfServiceStations]; - allocated dynamically

        // initialize instead of constructor, because shared data will be allocated in shared memory
        void initialize(const Settings &settings);
        char& getServiceStation(unsigned i);
    };

    namespace semaphore_index // index in semaphore set
    {
        static const unsigned short ITERATIONS_COUNTER = 0; // counting from 0 to numberOfIterationsToEnd
        static const unsigned short FREE_SERVICE_STATIONS = 1; // values from 0 to numberOfServiceStations
        static const unsigned short CRITICAL_SECTION = 2; // binary
        static const unsigned short ENTRANCE = 3; // binary
        static const unsigned short EXIT = 4; // binary
    }
}
