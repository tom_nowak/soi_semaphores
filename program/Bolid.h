#pragma once
#include <sys/time.h>

namespace soi
{
    class SharedData;
    class SemaphoreSet;
    class Settings;

    class Bolid
    {
    public:
        Bolid(SharedData &d, SemaphoreSet &semset, const Settings &s);
        ~Bolid();
        void run();

    private:
        // Data used in synchronization:
        SharedData &shd;
        SemaphoreSet &sem;
        const Settings &settings;
        int lastServiceStationUsed;
        // Data used in tests (mesuring time and number of iterations):
        unsigned numberOfIterations;
        unsigned long timeRoadway, timeRunway, timeService;
        const unsigned tRoadwayDiff, tRunwayDiff, tServiceDiff; // (max - min) from settings.getTimes()
        timeval startTime;

        void driveOnRoad();
        void goFromRoadToRunway();
        void driveOnRunway();
        void goFromRunwayToServiceStation();
        void stayAtServiceStation();
        void goFromServiceStationToRunway();
        void goFromRunwayToRoad();
        bool shouldFinish();
    };
}
