#pragma once
#include <initializer_list>
#include <sys/shm.h>

namespace soi
{
    class SemaphoreSet
    {
    public:
        SemaphoreSet(std::initializer_list<unsigned short> values); // should be used before forking processes
        ~SemaphoreSet();
        int getValue(unsigned short semaphoreNumber);
        void acquire(unsigned short semaphoreNumber); // acquire - also called: p, wait, down
        void release(unsigned short semaphoreNumber); // release - also called: v, signal, up

    private:
        const int semId;
        const pid_t creatorPid;
    };
}
