#include "SharedData.h"
#include <cstring>

void soi::SharedData::initialize(const Settings &settings)
{
    std::memset(this, 0, sizeof(SharedData) + settings.getNumberOfServiceStations()*sizeof(char));
    isRunwayFree = 1;
}

char& soi::SharedData::getServiceStation(unsigned i)
{
    char *c = (char*)(this + sizeof(SharedData) + i*sizeof(char));
    return *c;
}
